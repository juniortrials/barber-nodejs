const { User } = require('../models')

class AppointmentController {
  async create (req, res) {
    const provider = await User.findByPk(req.params.provider)
    console.log(provider.avatar)
    return res.render('appointments/create', { provider })
  }
}

module.exports = new AppointmentController()
